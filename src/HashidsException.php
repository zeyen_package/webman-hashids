<?php
declare (strict_types = 1);

namespace zeyen\hashids;

class HashidsException extends \Exception
{}
