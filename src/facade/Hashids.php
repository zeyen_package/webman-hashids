<?php
declare (strict_types = 1);

namespace zeyen\hashids\facade;

class Hashids
{
	protected static ?\zeyen\hashids\Hashids $_instance = null;

	public static function instance(): ?\zeyen\hashids\Hashids
    {
        //单例模式 - 禁用
//		if (!static::$_instance) {
			static::$_instance = new \zeyen\hashids\Hashids;
//		}

		return static::$_instance;
	}

	public static function __callStatic($name, $arguments)
	{
		return static::instance()->{$name}(... $arguments);
	}
}
