<?php
declare (strict_types = 1);

namespace zeyen\hashids;

use Hashids\Hashids as HashidsParent;

class Hashids
{
	/**
	 * config
	 *
	 * @var array
	 */
	protected array $config;

	/**
	 * The active modes instances.
	 *
	 * @var array<string,object>
	 */
	protected array $modes = [];

	/**
	 * Create a new hashids instance.
	 *
	 * @return void
     * @throws HashidsException
	 */
	public function __construct()
	{
		$this->config = config('plugin.zeyen.webman-hashids.app', []);

		if (empty($this->config) || empty($this->config['modes'])) {
			throw new HashidsException('Get configuration is null');
		}
	}

    /**
     * Get a mode instance.
     *
     * @param string|null $name
     * @param array $options
     * @return object
     * @throws HashidsException
     */
	public function mode(?string $name = null,array $options=[])
	{

		$name = $name ?: $this->getDefaultMode();
        $prefix = $options['prefix']??null;
        $salt = $options['salt']??null;
        $length = $options['length']??0;
        $alphabet = $options['alphabet']??null;
		if (!isset($this->modes[$name])) {
			$this->modes[$name] = $this->makeMode($name,$prefix,$salt,$length,$alphabet);
		}

		return $this->modes[$name];
	}

    /**
     * Make the mode instance.
     *
     * @param string $name
     * @param string|null $prefix Applicable to bilibili mode
     * @param string|null $salt
     * @param int $length
     * @param string|null $alphabet
     * @return object
     * @throws HashidsException
     */
	protected function makeMode(string $name, string|null $prefix = null, string|null $salt = null , int $length=0, string|null $alphabet = null): object
	{
		$config = $this->getModeConfig($name);

		if (!empty($name) && $name == 'bilibili') {
			return new Bilibili(
				$prefix ?? ($config['prefix'] ?: null)
			);
		}
		$config['salt'] = $salt?:$config['salt'] ?? '';
		$config['length'] = $length?:$config['length'] ?? 0;
		$config['alphabet'] = $alphabet?:$config['alphabet'] ?? 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		return new BaseHashids($config['salt'], $config['length'], $config['alphabet']);
	}

	/**
	 * Get the configuration for a mode.
	 *
	 * @param string|null $name
	 * 
	 * @return array
     * @throws HashidsException
	 */
	protected function getModeConfig(string $name = null): array
	{
		$name = $name ?: $this->getDefaultMode();
		$config = $this->config['modes'][$name] ?? [];

		if (!$config) {
			throw new HashidsException('Hashids modes ['. $name .'] not configured.');
		}

		return $config;
	}

	/**
	 * Get the default mode name.
	 *
	 * @return string
	 */
	protected function getDefaultMode(): string
	{
		return $this->config['default'] ?? 'main';
	}

    /**
     * Dynamically pass methods to the default mode.
     * @param string $method
     * @param array $parameters
     * @return mixed
     * @throws HashidsException
     */
	public function __call(string $method, array $parameters)
	{
		return $this->mode()->$method(...$parameters);
	}
}
