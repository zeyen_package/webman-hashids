<?php
declare (strict_types = 1);

use zeyen\hashids\facade\Hashids;

if (!function_exists('id_encode')) {
    /**
     * 生成加密ID
     *
     * @param mixed ...$arguments
     * @return string
     */
	function id_encode(...$arguments): string
    {
		if (count($arguments) === 1  && is_array($arguments[0])) {
			$arguments = $arguments[0];
		}
		// 先尝试拿最后一个数组为模式名
		$mode = array_pop($arguments);
		if (ctype_digit((string) $mode)) {
			$arguments = array_merge($arguments, [$mode]);
			$mode = null;
		}
        $options =[];
        // 尝试获取自定义参数
        if(isset($arguments[1]) && is_array($arguments[1])){
            $options = $arguments[1];
        }
		// B站模式，只拿第一个参数作为ID，如果传入多个忽略
		if ($mode === 'bilibili') {
			$id = array_shift($arguments);
			return Hashids::mode($mode,$options)->encode($id);
		}
		return Hashids::mode($mode,$options)->encode($arguments[0]);
	}
}

if (!function_exists('id_decode')) {
    /**
     * 解密生成的ID
     * @param string $string
     * @param string|null $mode
     * @param array $options
     * @return mixed
     */
	function id_decode(string $string, string|null $mode = null,array $options = []): mixed
    {
		if (!$mode) {
			$mode = config('plugin.zeyen.webman-hashids.app.default', null);
		}
        return Hashids::mode($mode,$options)->decode($string);
	}
}

if (!function_exists('id_mode')) {
    /**
     * 切换解密模式
     * @param string $name
     * @param array $options
     * @return mixed
     */
	function id_mode(string $name, array $options = []): mixed
    {
		return Hashids::mode($name,$options);
	}
}

if (!function_exists('id_build_alphabet')) {
	/**
	 * 获取字母表
	 * 
	 * @return string
	 */
	function id_build_alphabet(): string
    {
		return str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890');
	}
}
